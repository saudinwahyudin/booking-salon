package com.booking.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.workstage = workstage;
        this.reservationPrice = calculateReservationPrice();

        calculateReservationPrice();
    }

    private double calculateReservationPrice() {
        double totalPrice = 0;
    
        for (Service service : services) {
            totalPrice += service.getPrice();
        }
    
        // Apply discount based on customer membership
        if (customer.getMember().getMembershipName().equals("Silver")) {
            totalPrice *= 0.95; // 5% discount for Silver members
        } else if (customer.getMember().getMembershipName().equals("Gold")) {
            totalPrice *= 0.90; // 10% discount for Gold members
        }
    
        return totalPrice;
    }
    
}
