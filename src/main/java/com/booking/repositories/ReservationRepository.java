  package com.booking.repositories;

  import java.util.ArrayList;
  import java.util.Arrays;
  import java.util.List;
  import java.util.stream.Collectors;

  import com.booking.models.Customer;
  import com.booking.models.Employee;
  import com.booking.models.Reservation;
  import com.booking.models.Service;

  public class ReservationRepository {
      public static List<Reservation> getAllReservations(){
          List<Reservation> reservationList = new ArrayList<>();

          // Creating dummy reservations with "In Process" workstage
          Customer customer1 = (Customer) PersonRepository.getAllPerson().get(0);
          Employee employee1 = (Employee) PersonRepository.getAllPerson().get(4);
          List<Service> services1 = Arrays.asList(ServiceRepository.getAllService().get(0));

          Reservation reservation1 = new Reservation("Rsv-01", customer1, employee1, services1, "In Process");

          Customer customer2 = (Customer) PersonRepository.getAllPerson().get(1);
          Employee employee2 = (Employee) PersonRepository.getAllPerson().get(5);
          List<Service> services2 = Arrays.asList(ServiceRepository.getAllService().get(3), ServiceRepository.getAllService().get(4));

          Reservation reservation2 = new Reservation("Rsv-02", customer2, employee2, services2, "In Process");

      

          // Creating dummy reservations with "Finish" workstage
          Customer customer3 = (Customer) PersonRepository.getAllPerson().get(2);
          Employee employee3 = (Employee) PersonRepository.getAllPerson().get(6);
          List<Service> services3 = Arrays.asList(ServiceRepository.getAllService().get(2), ServiceRepository.getAllService().get(1));

          Reservation reservation3 = new Reservation("Rsv-03", customer3, employee3, services3, "Finish");


          // Creating dummy reservations with "Finish" workstage
          Customer customer4 = (Customer) PersonRepository.getAllPerson().get(2);
          Employee employee4 = (Employee) PersonRepository.getAllPerson().get(6);
          List<Service> services4 = Arrays.asList(ServiceRepository.getAllService().get(2));

          Reservation reservation4 = new Reservation("Rsv-03", customer3, employee3, services4, "Finish");

          reservationList.addAll(Arrays.asList(reservation1, reservation2, reservation3, reservation4));

          return reservationList;
      }

      public Reservation getReservationById(String reservationId) {
        for (Reservation reservation : getAllReservations()) {
            if (reservation.getReservationId().equals(reservationId)) {
                return reservation;
            }
        }
        return null; // Reservation not found
    }

        public List<Reservation> getAllReservationsInProcess() {
          return getAllReservations().stream()
                  .filter(reservation -> reservation.getWorkstage().equalsIgnoreCase("In Process"))
                  .collect(Collectors.toList());
      }

      public Customer getCustomerById(String customerId) {
        for (Reservation reservation : getAllReservations()) {
            if (reservation.getCustomer().getId().equals(customerId)) {
                return reservation.getCustomer();
            }
        }
        return null; // Customer not found
    }

    public Employee getEmployeeById(String employeeId) {
        for (Reservation reservation : getAllReservations()) {
            if (reservation.getEmployee().getId().equals(employeeId)) {
                return reservation.getEmployee();
            }
        }
        return null; // Employee not found
    }

    public void finishReservation(Reservation reservation) {
      reservation.setWorkstage("Finish");
  }

  public void cancelReservation(Reservation reservation) {
      reservation.setWorkstage("Cancel");
  }

  }
