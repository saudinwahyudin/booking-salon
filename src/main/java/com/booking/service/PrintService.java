package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.ReservationRepository;
import com.booking.repositories.ServiceRepository;

public class PrintService {

    private static List<Service> serviceList = ServiceRepository.getAllService();
    
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public String printServices(List<Service> serviceList){
        String result = "";
        
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-31s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+================================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-31s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public void showAllCustomer(List<Person> customerList){
        int num = 1;
        String line = "+====================================================================================+";
        System.out.printf("| %-4s | %-7s | %-11s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println(line);
        for (Person customer : customerList) {
            if(customer instanceof Customer){
                Customer  tmp = (Customer) customer;
                System.out.printf("| %-4s | %-7s | %-11s | %-15s | %-15s | %-15s |\n",
                num, customer.getId(), customer.getName(), customer.getAddress(),tmp.getMember().getMemberId(), tmp.getWallet());
                num++;
            }
        }

    }

    public void showAllEmployee(List<Person> employeeList){
        int num = 1;
        System.out.printf("| %-4s | %-7s | %-11s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("+==================================================================+");
        for (Person employee : employeeList) {
            if(employee instanceof Employee){
                Employee  tmp = (Employee) employee;
                System.out.printf("| %-4s | %-7s | %-11s | %-15s | %-15s |\n",
                num, employee.getId(), employee.getName(), employee.getAddress(),tmp.getExperience());
                num++;
            }
        }
    
    }

    public void showHistoryReservation(List<Reservation> reservationList){
        int num = 1;
        double totalProfit = 0; 

        String line = "+====================================================================================+";
        System.out.printf("| %-4s | %-7s | %-11s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "workstage");
        System.out.println(line);
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")){
                System.out.printf("| %-4s | %-7s | %-11s | %-15s | %-15s | %-15s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()),reservation.getReservationPrice(), reservation.getWorkstage());
                totalProfit += reservation.getReservationPrice();
                num++;
            }
        }
        System.out.println(line);
        System.out.printf("| %-60s | %-15s |\n", "Total Keuntungan", "Rp. " + totalProfit);
        System.out.println(line);
    }

    public void showService() {
        int num = 1;
        System.out.printf("| %-4s | %-11s | %-31s | %-15s |\n",
                "No.", "ID", "Service",  "Harga");
        System.out.println("+================================================================================================================+");
        for (Service service : serviceList) {
                System.out.printf("| %-4s | %-11s | %-31s | %-15s |\n",
                num, service.getServiceId(), service.getServiceName(), service.getPrice());
                num++;
            }
        }

}
