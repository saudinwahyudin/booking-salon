package com.booking.service;

import com.booking.models.*;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ReservationRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReservationService {
    PrintService printService = new PrintService(); 
    private final ReservationRepository reservationRepository;
    private final ValidationService validationService;
    private final Scanner scanner;

    public ReservationService(ReservationRepository reservationRepository, ValidationService validationService, Scanner scanner) {
        this.reservationRepository = reservationRepository;
        this.validationService = validationService;
        this.scanner = scanner;
    }

    public ReservationService(ReservationRepository reservationRepository, ValidationService validationService) {
        this.reservationRepository = reservationRepository;
        this.validationService = validationService;
        this.scanner = new Scanner(System.in);
    }

    public void createReservation() {
        // Input Customer ID
        String customerId;
        do {
            printService.showAllCustomer(PersonRepository.getAllPerson());
            System.out.println("Silahkan Masukkan Customer Id:");
            customerId = scanner.nextLine();
            if (!validationService.validateCustomerId(customerId)) {
                System.out.println("Customer yang dicari tidak tersedia.");
            }
        } while (!validationService.validateCustomerId(customerId));
    
        // Input Employee ID
        String employeeId;
        do {
            printService.showAllEmployee(PersonRepository.getAllPerson());
            System.out.println("Silahkan Masukkan Employee Id:");
            employeeId = scanner.nextLine();
            if (!validationService.validateEmployeeId(employeeId)) {
                System.out.println("Employee yang dicari tidak tersedia.");
            }
        } while (!validationService.validateEmployeeId(employeeId));
    
        // Input Services
        double totalBookingPrice = 0;
        List<Service> selectedServices = new ArrayList<>();
        boolean continueSelectingServices = true;
        while (continueSelectingServices) {
            printService.showService();
            System.out.println("Silahkan Masukkan Service Id:");
            String serviceId = scanner.nextLine();
            Service selectedService = validationService.validateServiceId(serviceId);
            if (selectedService != null) {
                totalBookingPrice += selectedService.getPrice();
                selectedServices.add(selectedService);
            }
    
            // Ask if the user wants to select another service
            System.out.println("Ingin pilih service yang lain (Y/T)?");
            String continueOption = scanner.nextLine().toUpperCase();
            continueSelectingServices = continueOption.equals("Y");
        }
    
        // Fetch Customer details from repository
        Customer customer = reservationRepository.getCustomerById(customerId);
    
        // Fetch Employee details from repository
        Employee employee =  reservationRepository.getEmployeeById(employeeId);
    
        // Create Reservation object with fetched details and selected services
        Reservation reservation = new Reservation(customerId, customer, employee, selectedServices, "In Process");
    
        // Display total booking price after applying member discount
        System.out.println("Booking Berhasil!");
        System.out.println("Total Biaya Booking : Rp. " + reservation.getReservationPrice());
    }
    


public void finishOrCancelReservation() {
    printService.showRecentReservation(reservationRepository.getAllReservations());

    System.out.println("Silahkan Masukkan Reservation Id:");
    String reservationId = scanner.nextLine();

    Reservation reservationToFinishOrCancel = reservationRepository.getReservationById(reservationId);
    if (reservationToFinishOrCancel == null) {
        System.out.println("Reservation not found.");
        return;
    }

    if (!reservationToFinishOrCancel.getWorkstage().equalsIgnoreCase("In Process")) {
        System.out.println("Reservation has already been finished or canceled.");
        return;
    }

    System.out.println("Selesaikan reservasi:");
    System.out.println("1. Finish");
    System.out.println("2. Cancel");
    int action = Integer.parseInt(scanner.nextLine());

    if (action == 1) {
        reservationRepository.finishReservation(reservationToFinishOrCancel);
        System.out.println("Reservasi dengan id " + reservationId + " sudah Finish");
    } else if (action == 2) {
        reservationRepository.cancelReservation(reservationToFinishOrCancel);
        System.out.println("Reservasi dengan id " + reservationId + " sudah Cancel");
    } else {
        System.out.println("Invalid action. Please enter '1' for Finish or '2' for Cancel.");
    }
}

private void printReservations(List<Reservation> reservations) {
    for (Reservation reservation : reservations) {
        System.out.println(reservation);
    }
}

}
