package com.booking.service;

import com.booking.models.Service;
import com.booking.models.Reservation;
import com.booking.repositories.ReservationRepository;

public class ValidationService {
    private static ReservationRepository reservationRepository;
    

    public ValidationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public boolean validateReservationId(String reservationId) {
        // Check if the reservation ID is not null or empty
        if (reservationId == null || reservationId.isEmpty()) {
            System.out.println("Reservation ID cannot be empty.");
            return false;
        }

        // Check if the reservation ID is unique
        boolean isUnique = reservationRepository.getAllReservations()
                .stream()
                .noneMatch(reservation -> reservation.getReservationId().equals(reservationId));

        if (!isUnique) {
            System.out.println("Reservation ID must be unique.");
            return false;
        }

        return true;
    }

    public boolean validateCustomerId(String customerId) {
        // Check if the customer ID is not null or empty
        if (customerId == null || customerId.isEmpty()) {
            System.out.println("Oh no! Customer ID cannot be empty.");
            return false;
        }
        
        // Check if the customer ID exists in the system
        boolean isCustomerExists = reservationRepository.getAllReservations()
                .stream()
                .anyMatch(reservation -> reservation.getCustomer().getId().equals(customerId));
        
        if (!isCustomerExists) {
            System.out.println("Oops! Customer with ID " + customerId + " does not exist.");
            return false;
        }
        
        return true;
    }
    
    public Service validateServiceId(String serviceId) {
        // Check if the service ID is not null or empty
        if (serviceId == null || serviceId.isEmpty()) {
            System.out.println("Service ID cannot be empty.");
            return null;
        }

        // Check if the service ID exists in the system by searching through reservations
        for (Reservation reservation : reservationRepository.getAllReservations()) {
            for (Service service : reservation.getServices()) {
                if (service.getServiceId().equals(serviceId)) {
                    return service;
                }
            }
        }

        System.out.println("Service ID not found.");
        return null;
    }

    public boolean validateEmployeeId(String employeeId) {
        // Check if the employee ID is not null or empty
        if (employeeId == null || employeeId.isEmpty()) {
            System.out.println("Employee ID cannot be empty.");
            return false;
        }

        // Check if the employee ID exists in the system (not implemented here)
        // You can add your implementation to check if the employee ID is valid

        return true;
    }
}
