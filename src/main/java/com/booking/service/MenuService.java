package com.booking.service;

import java.util.Scanner;

import com.booking.repositories.PersonRepository;
import com.booking.repositories.ReservationRepository;

public class MenuService {
    private static final Scanner input = new Scanner(System.in);
    private static final ReservationRepository reservationRepository = new ReservationRepository();
    private static final ValidationService validationService = new ValidationService(reservationRepository);

    public static void mainMenu() {
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/cancel reservation", "Exit"};

        boolean backToMainMenu = false;

        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            int optionMainMenu = Integer.valueOf(input.nextLine());
            switch (optionMainMenu) {
                case 0: 
                    backToMainMenu = true;
                    break;
                case 1:
                    showDataMenu();
                    break;
                case 2:
                    createReservation();
                    break;
                case 3:
                    finishOrCancelReservation();
                    break;
                case 4:
                    backToMainMenu = true;
                    break;
                default:
                    System.out.println("Invalid option. Please select again.");
            }
        } while (!backToMainMenu);
    }

    private static void showDataMenu() {
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "History Reservation", "Back to main menu"};
        boolean backToSubMenu = false;
        PrintService printService = new PrintService(); 

        do {
            PrintService.printMenu("Show Data", subMenuArr);
            int optionSubMenu = Integer.valueOf(input.nextLine());
            switch (optionSubMenu) {
                case 0: 
                    backToSubMenu = true;
                    break;
                case 1:
                    printService.showRecentReservation(reservationRepository.getAllReservations());
                    break;
                case 2:
                    printService.showAllCustomer(PersonRepository.getAllPerson());
                    break;
                case 3:
                    printService.showAllEmployee(PersonRepository.getAllPerson());
                    break;
                case 4:
                    printService.showHistoryReservation(reservationRepository.getAllReservations());
                    break;
                case 5:
                    backToSubMenu = true;
                    break;
                default:
                    System.out.println("Invalid option. Please select again.");
            }
        } while (!backToSubMenu);
    }

    private static void createReservation() {
        ReservationService reservationService = new ReservationService(reservationRepository, validationService);
        reservationService.createReservation();
    }

    private static void finishOrCancelReservation() {
        ReservationService reservationService = new ReservationService(reservationRepository, validationService);
        reservationService.finishOrCancelReservation();
    }
}
